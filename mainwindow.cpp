#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	// draw the editor component on the central widget
	editor = new QPlainTextEdit(ui->centralWidget);
	QVBoxLayout *layout = new QVBoxLayout;

	// add the editor to the vertical layout
	layout->addWidget(editor);
	// apply the layout on the central widget
	ui->centralWidget->setLayout(layout);

	connect(ui->actionSave_As, SIGNAL(triggered(bool)),
		this, SLOT(saveFile(bool)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::saveFile(bool)
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
		"",
		tr("Text files (*.txt)"));

	std::ofstream out(fileName.toStdString());

	out << editor->toPlainText().toStdString();
}
